package com.cedup.douglas.a03junho;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import android.widget.Spinner;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    Spinner estadosSp;
    ImageView imagens;
    String[] estados = {"Santa Catarina","Rio Grande do Sul", "Paraná"};
    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        estadosSp = findViewById(R.id.estadosSpinner);
        imagens = findViewById(R.id.imgbandeira);
        Intent intent = getIntent();
        String message = intent.getStringExtra("mail");
        Toast.makeText(this, "Bem vindo" + message, Toast.LENGTH_SHORT).show();
        
    }
}
