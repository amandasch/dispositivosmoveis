package com.cedup.douglas.a03junho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private static CharSequence target;
    EditText tx_email;
    EditText tx_senha;
    Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tx_email = findViewById(R.id.tx_email);
        tx_senha = findViewById(R.id.tx_senha);
        btn_login = findViewById(R.id.btn_login);


    }
}



    public final static boolean isValidEmail('CharSequencetarget') {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

        }
    }


    public void login(View v) {
        if (isValidEmail(tx_email.getText().toString())) {
            if(tx_email.getText().toString().equals("douglas.cedup@gmail.com")) &&
            tx_senha.getText().toString().equals("12345")){
                Toast.makeText(getApplicationContext(), "Login efetuado com sucesso", Toast.LENGTH_SHORT).show();

                Intent i = new Intent (this, Main2Activity.class);
                i.putExtra("mail", tx_email.getText().toString());
                startActivity(i);
            }

            else{
                Toast.makeText(getApplicationContext(),"Senha ou e-mail incorretos" ,Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getApplicationContext(),"Formato de e-mail incorreto" ,Toast.LENGTH_SHORT).show();

            }



        }

    }
}